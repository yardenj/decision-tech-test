﻿using System;
using System.Collections.Generic;
using app.Data;
using app.Services;
using Microsoft.Extensions.DependencyInjection;

namespace app
{
    class Program
    {
        static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
            .AddSingleton<IProductRepository,ProductRepository>()
            .AddSingleton<IOffersManager,OffersManager>()
            .AddSingleton<IBasketManager,BasketManager>()
            .BuildServiceProvider();           
        }
    }
}
