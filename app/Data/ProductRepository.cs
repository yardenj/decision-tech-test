using System;
using System.Collections.Generic;
using app.Model;

namespace app.Data
{
    public class ProductRepository : IProductRepository
    {
        public List<Product> List()
        {
            return new List<Product>{
                new Product(1,"Butter",0.8m),
                new Product(2,"Milk",1.15m),
                new Product(3,"Bread",1m)
            };
        }
    }
}
