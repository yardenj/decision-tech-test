using System;
using System.Collections.Generic;
using app.Model;

namespace app.Data
{
    public interface IProductRepository
    {
        List<Product> List();
    }
}
