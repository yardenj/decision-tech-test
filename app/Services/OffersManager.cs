using System;
using System.Collections.Generic;
using System.Linq;
using app.Model;

namespace app.Services
{
    public class OffersManager : IOffersManager
    {
        //for simple test - using list of func<> to calculate discounts for items
        //should read from DB or rule engine with proper DSL
        private List<Func<List<LineItem>, decimal>> offerRules;
        public OffersManager()
        {

            //Buy 2 Butter and get a Bread at 50% off
            Func<List<LineItem>, decimal> butterRule = lineItems =>
            {
                var butterItem = lineItems.Where(item => item.Product.Id == 1).FirstOrDefault();
                var breadItem = lineItems.Where(item => item.Product.Id == 3).FirstOrDefault();
                return butterItem != null && butterItem.Quantity >= 2 ? breadItem.Total * -0.5m : 0;
            };

            //Buy 3 Milk and get the 4th milk for free 
            Func<List<LineItem>, decimal> milkRule = lineItems =>
            {
                var milkItem = lineItems.Where(item => item.Product.Id == 2).FirstOrDefault();
                if (milkItem == null)
                    return 0;
                var discountedItems = (milkItem.Quantity / 4);
                return (decimal)discountedItems * -1 * milkItem.Product.Price; //for every 4 milk get 1 free
            };

            this.offerRules = new List<Func<List<LineItem>, decimal>> { butterRule, milkRule };
        }
        public decimal CalculateDiscountsForItems(List<LineItem> items)
        {
            decimal discount = 0;
            foreach (var rule in offerRules)
            {
                discount+=rule(items);
            }
            return discount;
        }
    }
}
