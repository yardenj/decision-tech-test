using System;
using System.Collections.Generic;
using app.Model;

namespace app.Services
{
    public interface IOffersManager
    {
        decimal CalculateDiscountsForItems(List<LineItem> items);
    }
}
