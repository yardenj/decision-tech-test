using System;
using System.Collections.Generic;
using System.Linq;
using app.Data;
using app.Model;

namespace app.Services
{
    public class BasketManager : IBasketManager
    {
        private IProductRepository _productRepository;
        private IOffersManager _offersManager;

        public BasketManager(IProductRepository productRepository, IOffersManager offersManager)
        {
            _productRepository = productRepository;
            _offersManager = offersManager;

        }
        public decimal CalculateTotalForItems(List<(int productId, int quantity)> items)
        {
            var basket = new Basket();
            var products = _productRepository.List();
            foreach (var item in items)
            {
                var product = products.Where(p => p.Id == item.productId).FirstOrDefault();
                if(product==null)
                    throw new Exception("Product Not Found");
                basket.AddItem(new LineItem(product, item.quantity));
            }
            var totalBasket = basket.Total;
            var totalDiscount = _offersManager.CalculateDiscountsForItems(basket.Items);
            return totalBasket + totalDiscount;
        }
    }
}
