using System;
using System.Collections.Generic;

namespace app.Services
{
    interface IBasketManager
    {
        decimal CalculateTotalForItems(List<(int productId,int quantity)> items);
    }
}
