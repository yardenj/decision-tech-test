using System;

namespace app.Model
{
    public class Product
    {
        public int Id { get;}
        public string Description { get;}
        public decimal Price { get;}

        public Product(int id,string description,decimal price)
        {
            Id = id;
            Description = description;
            Price = price;
        }
    }
}
