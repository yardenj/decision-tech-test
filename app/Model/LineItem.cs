using System;

namespace app.Model
{
    public class LineItem
    {
        public Product Product { get; }
        public int Quantity { get;set; }

        public decimal Total
        {
            get
            {
                return Product?.Price ?? 0 * Quantity;
            }
        }

        public LineItem(Product product, int quantity)
        {
            Product = product;
            Quantity = quantity;
        }
    }
}
