using System;
using System.Collections.Generic;
using System.Linq;

namespace app.Model
{
    public class Basket
    {
        private Dictionary<int, LineItem> _items;
        public List<LineItem> Items
        {
            get
            {
                return _items?.Values.ToList();
            }
        }

        public decimal Total
        {
            get
            {
                return Items?.Sum(li => li.Quantity * li.Product.Price) ?? 0;
            }
        }

        public Basket()
        {
            _items = new Dictionary<int, LineItem>();
        }
        public void AddItem(LineItem item)
        {
            if (_items.ContainsKey(item.Product.Id))
            {
                var currentItem = _items[item.Product.Id];
                currentItem.Quantity += item.Quantity;
            }
            else
            {
                _items[item.Product.Id] = item;
            }

        }
    }
}
