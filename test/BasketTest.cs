using System;
using app.Services;
using app.Data;
using Xunit;
using System.Collections.Generic;

namespace test
{
    public class BasketTest
    {
        private BasketManager _basketManager;

        public BasketTest()
        {
            IOffersManager offersManager = new OffersManager();
            IProductRepository productRepo = new ProductRepository();
            _basketManager = new BasketManager(productRepo, offersManager);

        }





        [Fact]
        public void OneBreadOneButterOneMilk()
        {
            var items = new List<(int productId, int quantity)>{
                (1,1),(2,1),(3,1)
            };
            Assert.Equal(2.95m, _basketManager.CalculateTotalForItems(items));
        }
        [Fact]
        public void TwoBreadTwoButter()
        {
            var items = new List<(int productId, int quantity)>{
                (1,2),(3,2)
            };
            Assert.Equal(3.1m, _basketManager.CalculateTotalForItems(items));
        }
        [Fact]
        public void FourMilk()
        {
            var items = new List<(int productId, int quantity)>{
                (2,4)
            };
            Assert.Equal(3.45m, _basketManager.CalculateTotalForItems(items));
        }
        [Fact]
        public void OneBreaTwoButterEightMilk()
        {
            var items = new List<(int productId, int quantity)>{
                (1,2),(2,8),(3,1)
            };
            Assert.Equal(9m, _basketManager.CalculateTotalForItems(items));
        }
        [Fact]
        public void ProductNotFoundException()
        {
            var items = new List<(int productId, int quantity)>{
                (8,1)
            };
            var ex = Assert.Throws<Exception>(()=>_basketManager.CalculateTotalForItems(items));
            Assert.Equal("Product Not Found",ex.Message);
        }

        /*

        All tests could be replaced with one test and multiple data objects to run, input+excpeted result
                [Theory]
                [MemberData(nameof(Data))]
                public void CalculateBasketTest(List<(int productId, int quantity)> items, decimal expected)
                {
                    Assert.Equal(expected, _basketManager.CalculateTotalForItems(items));
                }


                public static IEnumerable<object[]> Data =>
            new List<object[]>
            {
                    //  1 bread, 1 butter and 1 milk should be £9
                    new object[] { new List<(int productId, int quantity)>{(1,1),(2,1),(3,1)},2.95m},
                    // 2 butter and 2 bread should be £3.1
                    new object[] { new List<(int productId, int quantity)>{(1,2),(3,2)},3.1m},
                    // 4 milk should be £3.45
                    new object[] { new List<(int productId, int quantity)>{(2,4)},3.45m},
                    // 2 butter, 1 bread and 8 milk should be £9
                    new object[] { new List<(int productId, int quantity)>{(1,2),(2,8),(3,1)},9m}

            };
            */
    }

}
